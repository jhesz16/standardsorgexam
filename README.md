# StandardsOrgExam
Standards Org Exam  using cucumber

### What is this repository for? ###

* Exam of Jose Christopher Martin for Senior Automation Engineer
* Version 1.0

### How do I get set up? ###
1. Select a profile in Maven for the browser(CHrome will be the default browser if no profile will be selected)
2. Compile using maven
3. Run runner.java in src/test/java

### Prerequisite Plug ins ###
1. Cucumber for Java
2. Gherkin